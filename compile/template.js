const { _ludd } = require('./ludd')

/**
 * @methodType {_ludd.ludd}
 */
function ludd(config) {
  return _ludd(config)
}

module.exports = ludd

/* typal types/index.xml namespace */

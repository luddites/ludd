const { _ludd } = require('./ludd')

/**
 * The Luddites CLI Client To Install And Publish Dependencies.
 * @param {!_ludd.Config} config Options for the program.
 * @param {boolean} [config.shouldRun=true] A boolean option. Default `true`.
 * @param {string} [config.text] A text to return.
 * @return {Promise<string>}
 */
function ludd(config) {
  return _ludd(config)
}

module.exports = ludd

/* typal types/index.xml namespace */
/**
 * @typedef {_ludd.Config} Config `＠record` Options for the program.
 * @typedef {Object} _ludd.Config `＠record` Options for the program.
 * @prop {boolean} [shouldRun=true] A boolean option. Default `true`.
 * @prop {string} [text] A text to return.
 */

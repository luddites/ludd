<div align="center">

# ludd

%NPM: ludd%
<pipeline-badge/>

</div>

`ludd` is The Luddites CLI Client To Install And Publish Dependencies.

```sh
yarn add ludd
npm i ludd
```

## Table Of Contents

%TOC%

%~%
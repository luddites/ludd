## API

The package is available by importing its default function:

```js
import ludd from 'ludd'
```

%~%

<typedef method="ludd">types/api.xml</typedef>

<typedef>types/index.xml</typedef>

%EXAMPLE: example, ../src => ludd%
%FORK example%

%~%
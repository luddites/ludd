import { c } from 'erte'

/**
 * @type {_ludd.ludd}
 */
export default async function ludd(config = {}) {
  const {
    shouldRun = true,
    text = '',
  } = config
  if (!shouldRun) return ''
  console.log('ludd called with %s', c(text, 'yellow'))
  return text
}

/**
 * @suppress {nonStandardJsDocs}
 * @typedef {import('../types').ludd} _ludd.ludd
 */

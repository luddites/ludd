export {}

/* typal types/api.xml namespace */
/**
 * @typedef {_ludd.ludd} ludd The Luddites CLI Client To Install And Publish Dependencies.
 * @typedef {(config: !_ludd.Config) => string} _ludd.ludd The Luddites CLI Client To Install And Publish Dependencies.
 */

/**
 * @typedef {import('..').Config} _ludd.Config
 */
/**
 * @fileoverview
 * @externs
 */

/* typal types/index.xml externs */
/** @const */
var _ludd = {}
/**
 * Options for the program.
 * @record
 */
_ludd.Config
/**
 * A boolean option. Default `true`.
 * @type {boolean|undefined}
 */
_ludd.Config.prototype.shouldRun
/**
 * A text to return.
 * @type {string|undefined}
 */
_ludd.Config.prototype.text

/* typal types/api.xml externs */
/**
 * The Luddites CLI Client To Install And Publish Dependencies.
 * @typedef {function(!_ludd.Config): !Promise<string>}
 */
_ludd.ludd
